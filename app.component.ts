import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Hello World! Welcome to Precisely...';
  public empId = '';
  public fname = '';
  public lname = '';
  public pos = '';
  public rows: Array<{ empId: string, fname: string, lname: string, pos: string }> = [];
  res = '';

  buttonClicked() {
    if (this.empId != '' && this.fname != '' && this.lname != '' && this.pos != '') {
      this.empId = this.empId.toString();
      if (this.empId.length != 6) {
        this.res = "Invalid Emp_Id!!!";
      }
      else {
        if (!/[^a-zA-Z]/.test(this.fname) && !/[^a-zA-Z]/.test(this.lname) && !/[^a-zA-Z]/.test(this.pos)) {
          this.res = "Record added Successfully!!!";
          this.rows.push({ empId: this.empId, fname: this.fname, lname: this.lname, pos: this.pos }); // add new row from bottom
          // this.rows.unshift({ empId: this.empId, fname: this.fname, lname: this.lname, pos: this.pos });  // add new row from top
          this.empId = '';
          this.fname = '';
          this.lname = '';
          this.pos = '';
        }
        else {
          this.res = "Invalid name field!!!";
        }
      }
    }
    else {
      this.res = "All fields of input record are required!!!";
    }
  }
  deleteRow(id: number) {
    for (let i = 0; i < this.rows.length; ++i) {
      if (this.rows.indexOf(this.rows[i]) === id) {
        this.res = "Record with Emp_Id " + this.rows[i].empId + " deleted Successfully!!!"
        this.rows.splice(i, 1);
      }
    }
  }
}

